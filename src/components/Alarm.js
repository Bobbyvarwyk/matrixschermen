import React from 'react';
import { StyledAlarm } from './Alarm.styled.js';

function Alarm(props) {
    return (
        <StyledAlarm status={props.status} visible={props.visible} Position={props.Position}>
        </StyledAlarm>
    )
}

export default Alarm;
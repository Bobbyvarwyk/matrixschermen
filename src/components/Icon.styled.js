import styled from 'styled-components';
export { default } from './Icon';

export const StyledIcon = styled.div` 

    width: ${props =>
        props.status === 1 ? '700px' : '500px'
    };

    height: ${props =>
        props.status === 1 ? '700px' : '500px'
    };

    svg {
        width: 100%;
        height: 100%;
    }
    
    //color assigned to status
    path{
        fill: ${props =>
        props.status === 1 ? '#2ecc71' : 
        props.status === 2 ? '#fff9de' : 
        props.status === 3 ? '#d91e18' : 
        props.status === 4 ? '#d91e18' : 
        '#FFF9DE'
        };
    }
`;
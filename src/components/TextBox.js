import React from 'react';
import { StyledTextBox } from './TextBox.styled';

function TextBox(props) {
    var text = ""
    switch (props.status) {
        default:
        case 1:
            text = ""
        break;

        case 2:
            text = "1.5M afstand houden!"
        break;

        case 3:
            text = "Gangpad te druk"
        break;

        case 4:
            text = "Gangpad te druk"
        break;

    }

    if(text == "") {
        return null
    }

    return (
        <StyledTextBox>
            <h1>{text}</h1>
        </StyledTextBox>
    )
}

export default TextBox;
import styled from 'styled-components';
export { default } from './TextBox.js';

export const StyledTextBox = styled.div` 
    width: 100%;
    height: 20%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: flex-end;
    
    h1 {
        font-size: 6vw;
        margin: 0;
        text-transform: uppercase;
        font-family: 'Noto Sans', sans-serif;
        color: #ffec8b;
        letter-spacing: 2px;
    }
`;
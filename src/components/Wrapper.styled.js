import styled from 'styled-components';
export { default } from './Wrapper';

export const StyledWrapper = styled.div` 
    width: 100%;
    height:100%;
    background-color: #1B1B1B;
    padding: 75px;
    display: flex;
    flex-wrap: wrap;
    justify-content:center;
    align-items: center;
`;
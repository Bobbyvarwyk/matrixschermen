import React from 'react';
import { StyledWrapper } from './Wrapper.styled';

function Wrapper(props) {
    return (
        <StyledWrapper>
            {props.children}
        </StyledWrapper>
    )
}

export default Wrapper;
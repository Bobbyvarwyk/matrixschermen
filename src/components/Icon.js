import React from 'react';
import { StyledIcon } from './Icon.styled.js';
import {ReactComponent as CrossIcon} from '../img/cross.svg';
import {ReactComponent as ArrowIcon} from '../img/arrowdown.svg';
import {ReactComponent as DistanceIcon} from '../img/distance.svg';
import {ReactComponent as PeopleIcon} from '../img/people.svg';
import {ReactComponent as ShoppingcartIcon} from '../img/shoppingcart.svg';
import {ReactComponent as CashdeskIcon} from '../img/cashdesk.svg';


const Icon = (props) => {
    var iconToDisplay = (<CrossIcon />)
    switch (props.status) {
        default:
        case 1:
            iconToDisplay = (<ArrowIcon />)
            console.log(props.status)
        break;

        case 2:
            iconToDisplay = (<DistanceIcon />)
            console.log(props)
        break;

        case 3:
            iconToDisplay = (<CrossIcon />)
            console.log(props)
        break;

        case 4:
            iconToDisplay = (<CrossIcon />)
            console.log(props)
        break;

    }
    return (
        <StyledIcon status={props.status}>
            {iconToDisplay}
        </StyledIcon>
    )
}

export default Icon;
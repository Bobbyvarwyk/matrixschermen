import styled from 'styled-components';
export { default } from './Alarm';

export const StyledAlarm = styled.div` 
    width: 125px;
    height: 125px;
    background-color: #f89406;
    border-radius: 100%;
    box-shadow: 0px 0px 25px -5px #f89406;
    position: absolute;

    // transition for alarmsignal to switch
    opacity: ${({ visible }) => visible ? '1' : '0'};
    transition: 0.2s;

    // Show alarmsignal when status is not equal to one
    display: ${props =>
        props.status === 1 ? 'none' : 'flex'
    };
    
    
    // Positioning based on Position prop
    left: ${({ Position }) => Position ==='top-left' || Position === 'bottom-left' ? '20px' : ''};
    bottom: ${({ Position }) => Position ==='bottom-left' || Position === 'bottom-right' ? '20px' : ''};
    right: ${({ Position }) => Position ==='bottom-right' || Position === 'top-right' ? '20px' : ''};
    top: ${({ Position }) => Position ==='top-left' || Position === 'top-right' ? '20px' : ''};
`;
import React from 'react';
import { StyledIconGrid } from './IconGrid.styled.js';
import Icon from './Icon.js';
import Alarm from './Alarm.js';

class IconGrid extends React.Component {
    constructor( props ) {
      super( props )
      this.state = { 
        show : true,
       };
       this.blink = this.blink.bind(this)
    }

    blink() {
        this.setState({
            show : !this.state.show
        })
    }

    componentDidMount(){
            setInterval(this.blink, 1000)
    }

    render() {
        if(this.props.show){
            return null;
        }

        return (
            <StyledIconGrid>
                <Alarm visible={!this.state.show} Position="top-left" status={this.props.status}/>
                <Alarm visible={!this.state.show} Position="top-right" status={this.props.status}/>
                <Icon status={this.props.status}/>
                <Alarm visible={this.state.show} Position="bottom-left" status={this.props.status}/>
                <Alarm visible={this.state.show} Position="bottom-right" status={this.props.status}/>
            </StyledIconGrid>
        )
    }
}
export default IconGrid;
import styled from 'styled-components';
export { default } from './IconGrid';

export const StyledIconGrid = styled.div` 
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    height: 80%;
    position: relative;
    overflow: hidden;
    padding: 50px 180px;
`;
import React, { Component } from 'react';
import Wrapper from './components/Wrapper.js'; 
import IconGrid from './components/IconGrid.js';
import TextBox from './components/TextBox.js';

class App extends React.Component {
  constructor( props ) {
    super( props )
    this.state = { 
      show : true,
      status: 1,
      statusDescription: ""
     };
  }

  ws = new WebSocket("ws://127.0.0.1:3030/params?ID=RED1&type=output&treshold=1&cameraID=ABAB5");

  componentDidMount() {
    this.ws.onopen = () => {
      // If connected, log to console
      console.log("connected");
    }

    this.ws.onmessage = evt => {
      var responseFromSocket = JSON.parse(evt.data);
      this.setState({
        show: false,
        status: responseFromSocket.status,
        statusDescription: responseFromSocket.message
      })
    }
  }

  render() {
    return (
      <div className="App">
        <Wrapper>
            <IconGrid status={this.state.status}/>
            <TextBox status={this.state.status}> </TextBox>
        </Wrapper>
      </div>
    );
  }
}

export default App;
